import React from 'react'
import './BackButton.css'

const BackButton = () => {
  return (
    <div>
      <a className="button button--subtle" href="https://www.google.com">
        <span className="glyphicon glyphicon-arrow-left white"></span>
      </a>
    </div>
  )
  
}

export default BackButton
