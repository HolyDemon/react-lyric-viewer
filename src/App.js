import React, { Component } from 'react';
import BackButton from './components/BackButton/component'
import SideBar from './components/SideBar/component'
import './App.css';

class App extends Component {
  state = {
     shift: false
  }
  shiftPage = (e) => {
    this.setState({
      shift: !this.state.shift
    })
  }
  render() {
    return (
      <div className="page-with-contextual-sidebar">
        <div>
          <SideBar shiftPage={this.shiftPage}/>
        </div>
        <div>
          <BackButton shift={this.state.shift} />
        </div>
      </div>
    );
  }
}

export default App;
